using Discord.Commands;
using Mewdeko.Common.Attributes.TextCommands;
using Mewdeko.Modules.Utility.Services;
using System.Threading.Tasks;

namespace Mewdeko.Modules.Utility;

public partial class Utility
{
    [Group]
    public class NoituCommands : MewdekoSubmodule<UtilityService>
    {
        [Cmd, Aliases]
        public async Task Noituvietnam() => await ctx.Channel.SendConfirmAsync("Lệnh đã chạy").ConfigureAwait(false);

        [Cmd, Aliases]
        public async Task Noitutienganh() => await ctx.Channel.SendConfirmAsync("Lệnh đã chạy").ConfigureAwait(false);

        [Cmd, Aliases]
        public async Task Noitureset() => await ctx.Channel.SendConfirmAsync("Lệnh đã chạy").ConfigureAwait(false);
    }
}