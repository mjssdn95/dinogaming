using Discord.Commands;
using Mewdeko.Common.Attributes.TextCommands;
using Mewdeko.Modules.Utility.Services;
using System.Threading.Tasks;

namespace Mewdeko.Modules.Utility;

public partial class Utility
{
    [Group]
    public class DemsoCommands : MewdekoSubmodule<UtilityService>
    {
        [Cmd, Aliases]
        public async Task SetDemso() => await ctx.Channel.SendConfirmAsync("Lệnh đã chạy").ConfigureAwait(false);

        [Cmd, Aliases]
        public async Task Resetdemso() => await ctx.Channel.SendConfirmAsync("Lệnh đã chạy").ConfigureAwait(false);

    }
}