using Discord.Commands;
using Discord.Interactions;
using Fergun.Interactive;
using Fergun.Interactive.Pagination;
using Mewdeko.Common.Autocompleters;
using Mewdeko.Common.DiscordImplementations;
using Mewdeko.Common.Modals;
using Mewdeko.Modules.Help.Services;
using Mewdeko.Modules.Permissions.Services;
using System.Threading.Tasks;

namespace Mewdeko.Modules.Help;

[Discord.Interactions.Group("help", "Help Commands, what else is there to say?")]
public class HelpSlashCommand : MewdekoSlashModuleBase<HelpService>
{
    private readonly InteractiveService _interactivity;
    private readonly IServiceProvider _serviceProvider;
    private readonly GlobalPermissionService _permissionService;
    private readonly CommandService _cmds;
    private readonly GuildSettingsService _guildSettings;
    private readonly CommandHandler _ch;

    public HelpSlashCommand(
        GlobalPermissionService permissionService,
        InteractiveService interactivity,
        IServiceProvider serviceProvider,
        CommandService cmds,
        CommandHandler ch,
        GuildSettingsService guildSettings)
    {
        _permissionService = permissionService;
        _interactivity = interactivity;
        _serviceProvider = serviceProvider;
        _cmds = cmds;
        _ch = ch;
        _guildSettings = guildSettings;
    }

    [SlashCommand("help", "Shows help on how to use the bot")]
    public async Task Modules()
    {
        var embed = await Service.GetHelpEmbed(false, ctx.Guild, ctx.Channel, ctx.User);
        await RespondAsync(embed: embed.Build(), components: Service.GetHelpComponents(ctx.Guild, ctx.User).Build()).ConfigureAwait(false);
        try
        {
            var message = await ctx.Channel.GetMessagesAsync().FlattenAsync().ConfigureAwait(false);
            await HelpService.AddUser(message.FirstOrDefault(x => x.Author == ctx.User) as IUserMessage, DateTime.UtcNow).ConfigureAwait(false);
        }
        catch
        {
            // ignored
        }
    }

    [ComponentInteraction("helpselect", true)]
    public async Task HelpSlash(string[] selected)
    {
        var currentmsg = HelpService.GetUserMessage(ctx.User) ?? new MewdekoUserMessage
        {
            Content = "help",
            Author = ctx.User
        };
        var module = selected.FirstOrDefault();
        module = module?.Trim().ToUpperInvariant().Replace(" ", "");
        if (string.IsNullOrWhiteSpace(module))
        {
            await Modules().ConfigureAwait(false);
            return;
        }

        var prefix = await _guildSettings.GetPrefix(ctx.Guild);
        // Find commands for that module
        // don't show commands which are blocked
        // order by name
        var cmds = _cmds.Commands.Where(c =>
                c.Module.GetTopLevelModule().Name.ToUpperInvariant()
                    .StartsWith(module, StringComparison.InvariantCulture) && !_permissionService.BlockedCommands.Contains(c.Aliases[0].ToLowerInvariant()))
            .OrderBy(c => c.Aliases[0])
            .Distinct(new CommandTextEqualityComparer());
        // check preconditions for all commands, but only if it's not 'all'
        // because all will show all commands anyway, no need to check
        var succ = new HashSet<CommandInfo>((await Task.WhenAll(cmds.Select(async x =>
            {
                var pre = await x.CheckPreconditionsAsync(new CommandContext(ctx.Client, currentmsg), _serviceProvider).ConfigureAwait(false);
                return (Cmd: x, Succ: pre.IsSuccess);
            })).ConfigureAwait(false))
            .Where(x => x.Succ)
            .Select(x => x.Cmd));

        var cmdsWithGroup = cmds
            .GroupBy(c => c.Module.Name.Replace("Commands", "", StringComparison.InvariantCulture))
            .OrderBy(x => x.Key == x.First().Module.Name ? int.MaxValue : x.Count());

        if (!cmds.Any())
        {
            await ReplyErrorLocalizedAsync("module_not_found_or_cant_exec").ConfigureAwait(false);
            return;
        }

        var i = 0;
        var groups = cmdsWithGroup.GroupBy(_ => i++ / 48).ToArray();
        var paginator = new LazyPaginatorBuilder()
            .AddUser(ctx.User)
            .WithPageFactory(PageFactory)
            .WithFooter(PaginatorFooter.PageNumber | PaginatorFooter.Users)
            .WithMaxPageIndex(groups.Select(x => x.Count()).FirstOrDefault() - 1)
            .WithDefaultEmotes()
            .Build();

        await _interactivity.SendPaginatorAsync(paginator, ctx.Interaction as SocketInteraction,
            TimeSpan.FromMinutes(60)).ConfigureAwait(false);

        async Task<PageBuilder> PageFactory(int page)
        {
            await Task.CompletedTask.ConfigureAwait(false);
            var transformed = groups.Select(x => x.ElementAt(page).Select(commandInfo =>
                    $"{(succ.Contains(commandInfo) ? "✅" : "❌")}{prefix + commandInfo.Aliases[0],-15} {$"[{commandInfo.Aliases.Skip(1).FirstOrDefault()}]",-8}"))
                .FirstOrDefault();
            var last = groups.Select(x => x.Count()).FirstOrDefault();
            for (i = 0; i < last; i++)
            {
                if (i != last - 1 || (i + 1) % 1 == 0) continue;
                var grp = 0;
                var count = transformed.Count();
                transformed = transformed
                              .GroupBy(_ => grp++ % count / 2)
                              .Select(x => x.Count() == 1 ? $"{x.First()}" : string.Concat(x));
            }

            return new PageBuilder()
                .AddField(groups.Select(x => x.ElementAt(page).Key).FirstOrDefault(),
                    $"```css\n{string.Join("\n", transformed)}\n```")
                .WithDescription(
                    $"<:6891gwenlol:1182004603055779842>: Lệnh hiện tại của bạn là  {Format.Code(prefix)}\n <a:pickyes:1183894766648295476> : lệnh bạn có thể dùng .\n<a:pickno:1183894770834223164>: Bạn không thể dùng lệnh này .\n<:neko_holy:1182004479466422382>: Nếu bạn cần giúp đỡ bất cứ điều gì [The Support Server](https://discord.gg/C3yyk7ebEz)\nNhập `{prefix}h commandname` để xem thông tin lệnh")
                .WithImageUrl("https://media.discordapp.net/attachments/1120918668398825564/1209007570036072529/dino-spring-3.png?ex=65e55b0a&is=65d2e60a&hm=1efead2941f0dbdca9f610b929f38a3011f012d47feeb34ae27e8c6b4f0069a3&=&format=webp&quality=lossless&width=1193&height=671")
                .WithOkColor();
        }
    }
    [SlashCommand("invite", "You should invite me to your server and check all my features!")]
    public async Task Invite()
    {
        var eb = new EmbedBuilder()
            .AddField("Invite Link",
                "[Click Here](https://discord.com/oauth2/authorize?client_id=701019662795800606&scope=bot&permissions=66186303&scope=bot%20applications.commands)")
            .AddField("Website/Docs", "https://dinostar.vn")
            .AddField("Support Server", "https://discord.gg/dinogaming")
            .WithImageUrl("https://media.discordapp.net/attachments/1120918668398825564/1209007570036072529/dino-spring-3.png?ex=65e55b0a&is=65d2e60a&hm=1efead2941f0dbdca9f610b929f38a3011f012d47feeb34ae27e8c6b4f0069a3&=&format=webp&quality=lossless&width=1193&height=671")
            .WithOkColor();
        await ctx.Interaction.RespondAsync(embed: eb.Build()).ConfigureAwait(false);
    }

    [SlashCommand("search", "get information on a specific command")]
    public async Task Search
    (
        [Discord.Interactions.Summary("command", "the command to get information about"), Autocomplete(typeof(GenericCommandAutocompleter))] string command
    )
    {
        var com = _cmds.Commands.FirstOrDefault(x => x.Aliases.Contains(command));
        if (com == null)
        {
            await Modules().ConfigureAwait(false);
            return;
        }
        var comp = new ComponentBuilder().WithButton(GetText("help_run_cmd"), $"runcmd.{command}", ButtonStyle.Success, disabled: com.Parameters.Count != 0);

        var embed = await Service.GetCommandHelp(com, ctx.Guild);
        await RespondAsync(embed: embed.Build(), components: comp.Build()).ConfigureAwait(false);
    }

    [ComponentInteraction("runcmd.*", true)]
    public async Task RunCmd(string command)
    {
        var com = _cmds.Commands.FirstOrDefault(x => x.Aliases.Contains(command));
        if (com.Parameters.Count == 0)
        {
            _ch.AddCommandToParseQueue(new MewdekoUserMessage
            {
                Content = await _guildSettings.GetPrefix(ctx.Guild) + command,
                Author = ctx.User,
                Channel = ctx.Channel
            });
            _ = Task.Run(() => _ch.ExecuteCommandsInChannelAsync(ctx.Channel.Id)).ConfigureAwait(false);
            return;
        }

        await RespondWithModalAsync<CommandModal>($"runcmdmodal.{command}").ConfigureAwait(false);
    }

    [ModalInteraction("runcmdmodal.*", ignoreGroupNames: true)]
    public async Task RunModal(string command, CommandModal modal)
    {
        await DeferAsync().ConfigureAwait(false);
        var msg = new MewdekoUserMessage
        {
            Content = $"{await _guildSettings.GetPrefix(ctx.Guild)}{command} {modal.Args}",
            Author = ctx.User,
            Channel = ctx.Channel
        };
        _ch.AddCommandToParseQueue(msg);
        _ = Task.Run(() => _ch.ExecuteCommandsInChannelAsync(ctx.Channel.Id)).ConfigureAwait(false);
    }
    [ComponentInteraction("toggle-descriptions:*,*", true)]
    public async Task ToggleHelpDescriptions(string sDesc, string sId)
    {
        if (ctx.User.Id.ToString() != sId) return;

        await DeferAsync().ConfigureAwait(false);
        var description = bool.TryParse(sDesc, out var desc) && desc;
        var message = (ctx.Interaction as SocketMessageComponent)?.Message;
        var embed = await Service.GetHelpEmbed(description, ctx.Guild, ctx.Channel, ctx.User);

        await message.ModifyAsync(x => { x.Embed = embed.Build(); x.Components = Service.GetHelpComponents(ctx.Guild, ctx.User, !description).Build(); }).ConfigureAwait(false);
    }

    // LOL
    [SlashCommand("lol", "Find Team LEAGUE OF LEGENDS")]
    public async Task FindTeamLOL(
    [
    Choice("Sắt", "IRON"),
    Choice("Đồng", "BRONZE"),
    Choice("Bạc", "SILVER"),
    Choice("Vàng", "GOLD"),
    Choice("Bạch Kim", "PLATIUM"),
    Choice("Lục Bảo","EMERALD"),
    Choice("Kim Cương", "DIAMOND"),
    Choice("Cao Thủ", "MASTER"),
    Choice("Đại Cao Thủ", "GRANDMASTER"),
    Choice("Thách Đấu", "CHALLENGER"),
    Choice("Aram", "ARAM"),
    Choice("Đánh Thường", "NORMAL"),
    Choice("[Chế Độ Giới Hạn] URF", "URF")
    ]
    string LOLMode,
    string Msg = "")
    {
        if (ctx.User is not IVoiceState voiceState || voiceState.VoiceChannel == null)
        {
            await ctx.Interaction.RespondAsync("Need to be in a voice channel to use this command.");
            return;
        }

        var voiceChannel = voiceState.VoiceChannel;
        var users = await voiceChannel.GetUsersAsync().ToListAsync();
        var userCount = users.Count;
        var channelName = voiceChannel.Name;
        var slotValue = userCount == 0 ? "∞" : $"{userCount} / {(voiceChannel.UserLimit.HasValue ? voiceChannel.UserLimit.ToString() : "unlimited")}";
        var invite = await voiceChannel.CreateInviteAsync(maxAge: (int)TimeSpan.FromHours(1).TotalSeconds);
        var inviteUrl = invite.Url;

        var thumbnailUrl = GetThumbnailUrl("lol", LOLMode); // Lấy URL cho thumbnail dựa trên rank mode

        var builder = new ComponentBuilder()
            .WithButton($"Tham gia : {channelName}", style: ButtonStyle.Link, url: inviteUrl, emote: "<:icons_lol:1206391029608091679>".ToIEmote())
            .WithButton("Discord Hỗ Trợ", url: "https://discord.gg/dinogaming", style: ButtonStyle.Link, emote: "<a:thongbso:1200132023302500453>".ToIEmote());
        var eb = new EmbedBuilder()
            .WithAuthor(ctx.User.Username, ctx.User.GetAvatarUrl(), ctx.User.GetAvatarUrl())
            .AddField("> **Room**", $"**> {channelName}**", inline: true)
            .AddField("> **Slots**", $"**> {slotValue}**", inline: true)
            .AddField("> **Rank/Mode**", $"**> {LOLMode}**", inline: true)
            .WithThumbnailUrl(thumbnailUrl) // Đặt URL cho thumbnail
            .WithFooter("/help [lol] [msg]- để tìm đồng đội")
            .WithTimestamp(DateTime.Now)
            .WithOkColor();
        var messageContent = $"**{ctx.User.Mention}{Msg}**";

        await ctx.Interaction.RespondAsync(messageContent, embed: eb.Build(), components: builder.Build()).ConfigureAwait(false);
    }
    // VALORANT 
    [SlashCommand("val", "Find Team VALORANT")]
    public async Task FindTeamVAL(
    [
    Choice("Iron","Iron"),
    Choice("Bronze","Bronze"),
    Choice("Silver","Silver"),
    Choice("Gold ","Gold"),
    Choice("Platinum","Platinum"),
    Choice("Diamond","Diamond"),
    Choice("Ascendant","Ascendant"),
    Choice("Immortal","Immortal"),
    Choice("Radiant","Radiant"),
    Choice("Unrated","Unrated"),
    Choice("Spike Rush","Spike Rush")
    ]
        string VALMode,
        string Msg = "")
    {
        if (ctx.User is not IVoiceState voiceState || voiceState.VoiceChannel == null)
        {
            await ctx.Interaction.RespondAsync("Need to be in a voice channel to use this command.");
            return;
        }

        var voiceChannel = voiceState.VoiceChannel;
        var users = await voiceChannel.GetUsersAsync().ToListAsync();
        var userCount = users.Count;
        var channelName = voiceChannel.Name;
        var slotValue = userCount == 0 ? "∞" : $"{userCount} / {(voiceChannel.UserLimit.HasValue ? voiceChannel.UserLimit.ToString() : "unlimited")}";
        var invite = await voiceChannel.CreateInviteAsync(maxAge: (int)TimeSpan.FromHours(1).TotalSeconds);
        var inviteUrl = invite.Url;
        var thumbnailUrl = GetThumbnailUrl("val", VALMode.ToLower());
        // Lấy URL cho thumbnail dựa trên rank mode
        var builder = new ComponentBuilder()
            .WithButton($"Tham gia : {channelName}", style: ButtonStyle.Link, url: inviteUrl, emote: "<:LVT_Valorant:1209937250595508264>".ToIEmote())
            .WithButton("Discord Hỗ Trợ", url: "https://discord.gg/dinogaming", style: ButtonStyle.Link, emote: "<a:thongbso:1200132023302500453>".ToIEmote());
        var eb = new EmbedBuilder()
            .WithAuthor(ctx.User.Username, ctx.User.GetAvatarUrl(), ctx.User.GetAvatarUrl())
            .AddField("> **Room**", $"**> {channelName}**", inline: true)
            .AddField("> **Slots**", $"**> {slotValue}**", inline: true)
            .AddField("> **Rank/Mode**", $"**> {VALMode}**", inline: true)
            .WithThumbnailUrl(thumbnailUrl) // Đặt URL cho thumbnail
            .WithFooter("/help val - để tìm đồng đội")
            .WithTimestamp(DateTime.Now)
            .WithOkColor();
        var messageContent = $"**{ctx.User.Mention}{Msg}**";
        await ctx.Interaction.RespondAsync(messageContent, embed: eb.Build(), components: builder.Build()).ConfigureAwait(false);
    }
    public string GetThumbnailUrl(string game, string rank)
    {
        Console.WriteLine($"Game: {game}, Rank: {rank}");
        if (game.ToLower() == "lol")
        {
            switch (rank.ToLower())
            {
                case "sắt":
                case "iron":
                    return "https://raw.communitydragon.org/latest/plugins/rcp-fe-lol-static-assets/global/default/images/ranked-mini-crests/iron.png";
                case "đồng":
                case "bronze":
                    return "https://raw.communitydragon.org/latest/plugins/rcp-fe-lol-static-assets/global/default/images/ranked-mini-crests/bronze.png";
                case "bạc":
                case "silver":
                    return "https://raw.communitydragon.org/latest/plugins/rcp-fe-lol-static-assets/global/default/images/ranked-mini-crests/silver.png";
                case "vàng":
                case "gold":
                    return "https://raw.communitydragon.org/latest/plugins/rcp-fe-lol-static-assets/global/default/images/ranked-mini-crests/gold.png";
                case "bạch Kim":
                case "platium":
                    return "https://raw.communitydragon.org/latest/plugins/rcp-fe-lol-static-assets/global/default/images/ranked-mini-crests/platinum.png";
                case "lục bảo":
                case "emerald":
                    return "https://images.contentstack.io/v3/assets/blt731acb42bb3d1659/bltffdd214e7a3cd51e/65496017970384040a35792b/Emblems_emerald.png";
                case "kim cương":
                case "diamond":
                    return "https://raw.communitydragon.org/latest/plugins/rcp-fe-lol-static-assets/global/default/images/ranked-mini-crests/diamond.png";
                case "cao thủ":
                case "master":
                    return "https://raw.communitydragon.org/latest/plugins/rcp-fe-lol-static-assets/global/default/images/ranked-mini-crests/master.png";
                case "đại cao thủ":
                case "grandmaster":
                    return "https://raw.communitydragon.org/latest/plugins/rcp-fe-lol-static-assets/global/default/images/ranked-mini-crests/grandmaster.png";
                case "thách đấu":
                case "challenger":
                    return "https://raw.communitydragon.org/latest/plugins/rcp-fe-lol-static-assets/global/default/images/ranked-mini-crests/challenger.png";
                case "aram":
                    return "https://images-ext-2.discordapp.net/external/hb1GA1JuZeD8LRgVJsKT7uhjmVTPsDGm1Pqqb_5G-u8/https/bettergamer.fra1.cdn.digitaloceanspaces.com/media/uploads/788e655d1bc017b4c2841d21c676b7d2.png?format=webp&quality=lossless&width=671&height=671";
                case "urf":
                    return "https://static.wikia.nocookie.net/leagueoflegends/images/2/2e/The_Thinking_Manatee_profileicon.png/revision/latest/smart/width/250/height/250?cb=20170504215412";
                case "đánh thường":
                case "normal":
                    return "https://cdn.discordapp.com/attachments/855800833279131648/1206632240247873566/190c5f6f7f32104a60a0727ed501aaa1.png?ex=65dcb6d8&is=65ca41d8&hm=944520fa3f2d57ba7551f8b8b3b1e0f58278333d3fcdb647cf7558d220c82ff5&";
                default:
                    return "";
            }
        }
        else if (game.ToLower() == "val")
        {
            switch (rank.ToLower())
        {
            case "iron":
                return "https://cdn.discordapp.com/attachments/1209929161506824314/1209931605167050814/Iron.png?ex=65e8b79e&is=65d6429e&hm=d65d9baee53227e3f995f7e04437ac30fabd013828abd02667b80043d4b31a54&";
            case "bronze":
                return "https://cdn.discordapp.com/attachments/1209929161506824314/1209931571814076506/BRONZE.png?ex=65e8b796&is=65d64296&hm=e30c2ae5709eecbeb597029dfa88daf0d0fec9e3d4ad62355d3e74e51c393626&";
            case "silver":
                return "https://cdn.discordapp.com/attachments/1209929161506824314/1209932137675890818/silver.png?ex=65e8b81d&is=65d6431d&hm=b4c87541ab9d84d83712d9663ed29b254533b4512ed66ca04b306df203e213ca&";
            case "gold":
                return "https://cdn.discordapp.com/attachments/1209929161506824314/1209932161835204608/golden.png?ex=65e8b822&is=65d64322&hm=942f60d3f9ab2b9afe50305964202d6a52ff3f19e54775d510130c85b027ce99&";
            case "platinum":
                return "https://cdn.discordapp.com/attachments/1209929161506824314/1209932586885840968/platium.png?ex=65e8b888&is=65d64388&hm=aa0bad496e04a13f64a8740f3f980e38aee7e45f3347df05fda72f71dfdee4d1&";
            case "diamond":
                return "https://cdn.discordapp.com/attachments/1209929161506824314/1209932744277106758/diamond.png?ex=65e8b8ad&is=65d643ad&hm=c3d67053f639ac51295778d2fbc69e4f7a0b1b49f8943db410c29686f2faf8cf&";
            case "ascendant":
                return "https://cdn.discordapp.com/attachments/1209929161506824314/1209934842045796433/ascendant.png?ex=65e8baa1&is=65d645a1&hm=c4454c5f24f4c9c88621e9b5ecc088e0fc323cb89a4fa352b658631440ff40c0&";
            case "immortal":
                return "https://cdn.discordapp.com/attachments/1209929161506824314/1209932842344128523/IMMORTAL.png?ex=65e8b8c5&is=65d643c5&hm=f3ff18ac64440960becbe6326461a2a181759edb3387c7411c6eb960df217263&";
            case "radiant":
                return "https://cdn.discordapp.com/attachments/1209929161506824314/1209933162076049438/RADIANT.png?ex=65e8b911&is=65d64411&hm=c88648088699aab9f4b16c1a33e444b05ca4c55fe63acbf3ea84fa85f93329eb&";
            case "unrated":
                return "https://cdn.discordapp.com/attachments/1209929161506824314/1209935049630416896/unrated.png?ex=65e8bad3&is=65d645d3&hm=ccf35d851237ea902fb93d0b06b8d37d7de6874ae14e6bf385842aed9cf5cb94&";
            case "spike rush":
                return "https://cdn.discordapp.com/attachments/1209929161506824314/1209935049630416896/unrated.png?ex=65e8bad3&is=65d645d3&hm=ccf35d851237ea902fb93d0b06b8d37d7de6874ae14e6bf385842aed9cf5cb94&";
            default:
                return "";
        }
        }
        else
        {
            return "Trò chơi không hợp lệ";
        }
    }
}
